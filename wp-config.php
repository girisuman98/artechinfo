<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'artechinfo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xvRRNn+<xW*o^TXUL`]M8(w+Ky^b Mr*NPvL0GDe*5qxIH{&6h)0R;cS#]y4{f!h' );
define( 'SECURE_AUTH_KEY',  'q;/):5)dAVd}[Vr-h.V^GF7GS{YxenfKwT)nFPxVmI|kJ_9BC25hs[MH<%R2mkpG' );
define( 'LOGGED_IN_KEY',    'BHUPX}=4<]+MUY*8jBGGmSG>^9ip</_>25v{6pfT6~-quo}14,tCDJu-IVP[&SyN' );
define( 'NONCE_KEY',        '~`Ek.jV*QIj[b(]DgE+b!..iE*nzH !K$#G_Y~6,qr;pl(=A3LggM:v?DRQZ:}>S' );
define( 'AUTH_SALT',        'gvI|hr8QXXBI=F^(K=(*w6!<H{WoGxhu$utG/}%A5Lq1V*,@@8~o5~l9AN_r(>)j' );
define( 'SECURE_AUTH_SALT', 'l54Q^!ajUxLv_V|};;kwbVj-v)~CKXSPwj?cntl|547}YlJS]/tFVb9Cl_:CNt^2' );
define( 'LOGGED_IN_SALT',   '^R>EmGuUjFYNW<B}C9:Ull]j/tB0cIE_:084Qg`))$SFmQzNVggTqe<d#8U_mm5;' );
define( 'NONCE_SALT',       '$ECyNMPQ=1##~ kEtC]/&( ]c[N9L*(?o{B8MJf%O9_]n=|b}D;TEp]QtSVD($=}' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'artechinfo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
