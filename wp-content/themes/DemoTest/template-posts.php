<?php
/**
 * Template Name: Post listing
 *
 * @package fashionclaire
 */

get_header(); 
?>

<?php 
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
 
 <div class="container">
 	<div class="row">
		<div class="col-sm-12">
			<h2>Categories</h2>
		</div>
		<?php 
		$categories = get_categories();
		foreach($categories as $category) {
		   echo '<div class="col-md-2 card"><a class="getPosts" data-id="' . $category->term_id . '">' . $category->name . '(' .$category->category_count. ')</a></div>';
		}
		?>
	</div>
	<input type="hidden" id="ajaxUrl" value="<?php echo admin_url('admin-ajax.php'); ?>" />
	<?php if ( $wpb_all_query->have_posts() ) : ?>
	<div class="row mt-5">
		<div class="col-sm-12">
			<h2>Posts</h2>
		</div>
	</div>
	<div class="row justify-content-center" id="posts">
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
        <div class="card col-md-5-cols mt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
    <?php endwhile; ?>
    <!-- end of the loop -->
 
	</div>
	<?php wp_reset_postdata(); ?>
 
	<?php else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
	
	<div class="row mt-5">
		<div class="col-sm-12">
			<h2>Categories Listing</h2>
		</div>
		<div class="col-sm-12">
			<?php 
				$ajaxposts = get_posts( array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1) );
			?>
			<table class="table mt-3">
				  <thead>
					<tr>
					  <th scope="col">#</th>
					  <th scope="col">Post title</th>
					  <th scope="col">Category name</th>
					  <th scope="col">Post date</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($ajaxposts as $key => $post){ ?>
					<tr>
					  <th scope="row"> <?php echo $key + 1; ?></th>
					  <td><?php echo $post->post_title; ?></td>
					  <td><?php echo get_the_category( $post->ID )[0]->name; ?></td>
					  <td><?php echo $post->post_date; ?></td>
					</tr>
				  <?php } ?>
				</tbody>
			</table>		
		</div>
	</div>
	
	<div class="row mt-5">
		<div class="col-sm-12">
			<h2>Top 3 Categories Listing</h2>
		</div>
		<div class="col-sm-12">
			<?php 
				$categoriesList = get_categories([
					'orderby'  => 'count',
					'order'    => 'DESC',
					'limit'    => 3
				]);
			?>
			<table class="table mt-3">
				  <thead>
					<tr>
					  <th scope="col">#</th>
					  <th scope="col">Category name</th>
					  <th scope="col">Total Post Count</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($categoriesList as $key => $cat){ ?>
					<tr>
					  <th scope="row"> <?php echo $key + 1; ?></th>
					  <td><?php echo $cat->name; ?></td>
					  <td><?php echo $cat->category_count; ?></td>
					</tr>
				  <?php if (++$key == 3) break; } ?>
				</tbody>
			</table>		
		</div>
	</div>
</div>
 
<br /><br /><br /><br /><br /><br />
<?php get_footer(); ?>