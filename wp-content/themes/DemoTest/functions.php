<?php
/*This file is part of DemoTest, fashionclaire child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

if ( ! function_exists( 'suffice_child_enqueue_child_styles' ) ) {
	function DemoTest_enqueue_child_styles() {
	    // loading parent style
	    wp_register_style(
	      'parente2-style',
	      get_template_directory_uri() . '/style.css'
	    );

	    wp_enqueue_style( 'parente2-style' );
	    // loading child style
	    wp_register_style(
	      'childe2-style',
	      get_stylesheet_directory_uri() . '/style.css'
	    );
	    wp_enqueue_style( 'childe2-style');
		wp_enqueue_script('jqueryUI', '//code.jquery.com/ui/1.11.4/jquery-ui.js', array(), null, true);
		wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', array(), null, true);
	 }
}
add_action( 'wp_enqueue_scripts', 'DemoTest_enqueue_child_styles' );

/*Write here your own functions */

add_action( 'wp_ajax_nopriv_ajx_handle_posts_by_category_action', 'ajx_handle_posts_by_category_action' );

function ajx_handle_posts_by_category_action() {
	$data = null;
    $category_id = (int)$_POST['cat_id'];
	$ajaxposts = get_posts( array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1, 'category' => $_POST['cat_id']) );
	if($ajaxposts){
		foreach($ajaxposts as $post){
			$data .= '<div class="card col-md-5-cols mt-3"><a href="/artechinfo/'. $post->post_name. '">'.$post->post_title.'</a></div>';
		}
	}
	echo $data;
    wp_die();
}