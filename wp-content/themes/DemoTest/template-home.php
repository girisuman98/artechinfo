<?php
/**
 * Template Name: Homepage
 *
 * @package fashionclaire
 */

get_header(); 
$emailError = $ageError = "";
$errFlag = 0;
$isSuccess = 0;
$message = "";
if(isset($_POST["submit"])){
	global $wpdb;
	$table_name = "artechinfo_user_details";
	
	// Email validate
	if(isset($_POST["email"]) && $_POST["email"] != ""){
		$email = htmlspecialchars($_POST["email"]);
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$emailError = "Email address is invalid";
			$errFlag = 1;
		}
	} else {
		$emailError = "Email address is required";
		$errFlag = 1;
	}
	
	// Age validate
	if(isset($_POST["ageValue"]) && $_POST["ageValue"] != ""){
		$age = htmlspecialchars($_POST["ageValue"]);
		
		if(!is_float($age) && !is_numeric($age)){
			$ageError = "Age should be decimal only";
			$errFlag = 1;
		}
		if($age < 10 || $age > 100){
			$ageError = "Age should be between 10 to 100 years";
			$errFlag = 1;
		}
	} else {
		$ageError = "Age is required";
		$errFlag = 1;
	}
	
	if($errFlag == 0){
		
		$data = array('email' => $email, 'age' => $age);
		$format = array('%s','%s');
		$wpdb->insert($table_name, $data, $format);
		$last_id = $wpdb->insert_id;
		
		if($last_id){
			$isSuccess = 1;
			$message = "Success";
		} else {
			$message = "Some error occured. Please try after sometime";
		}
	} else {
		$message = "Rectify the errors";
	}
}
?>
    <div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
			<?php if($isSuccess == 1 && $message != "") { ?>
				<div class="alert alert-success" role="alert">
				  <?php echo $message; ?>
				</div>
			<?php } if($isSuccess == 0 && $message != "") { ?>
				<div class="alert alert-danger" role="alert">
				  <?php echo $message; ?>
				</div>
			<?php } ?>
				<form method="post" id="userDetails" action="">
				  <div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" <?php if(isset($_POST["email"]) && $_POST["email"] != "" && $isSuccess != 1){ ?> value="<?php echo $_POST['email']; ?>" <?php } ?> id="email" name="email" placeholder="Enter email" required>
					<?php if($emailError) { ?> <p class="error"> <?php echo $emailError; ?> </p> <?php } ?>
				  </div>
				  <br />
				  <div class="form-group">
					<label for="age">Age</label>
					<p id="age"></p>
					<input type="hidden" id="ageValue" name="ageValue" />
					<?php if($ageError) { ?> <p class="error"> <?php echo $ageError; ?> </p> <?php } ?>
				  </div>
				  <br />
				  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
				</form>
				<br /><br /><br /><br /><br />
			</div>
		</div>
	</div>
    
<?php get_footer(); ?>