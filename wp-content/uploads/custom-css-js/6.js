<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
/* Default comment here */ 
jQuery(document).ready(function( $ ){
  	var tooltip = $('<div id="tooltip" />').css({
        position: 'absolute',
        top: -25,
        left: -10
    }).hide();
    $( "#age" ).slider({
         min: 10,
         max: 100,
         step:0.5,
         slide: function(event, ui) {
            tooltip.text(ui.value);
           $("#ageValue").val(ui.value);
        },
        change: function(event, ui) {}
    }).find(".ui-slider-handle").append(tooltip).hover(function() {
        tooltip.show()
    }, function() {
        tooltip.hide()
    });
  $( ".getPosts" ).click(function() {
    if(jQuery(this).attr("data-id") == "")
      return false;
    jQuery.ajax({
      url: $("#ajaxUrl").val(),
      data: { "cat_id": jQuery(this).attr("data-id"),  action: 'ajx_handle_posts_by_category_action'},
      type: "post",
      success: function(data){
        $("#posts").html(data);
      }
    });
  });
});</script>
<!-- end Simple Custom CSS and JS -->
